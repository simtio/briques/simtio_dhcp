const gulp = require('gulp');
const tar = require('gulp-tar-path');
const gzip = require('gulp-gzip');
const clean = require('gulp-clean');
const browserify = require('gulp-browserify');
const sass = require('gulp-sass');
const package = require('./package.json');

gulp.task('tar', () =>
    gulp.src(['locales', 'views', 'docker-compose.yml', '*.py', 'assets/css/*.css', 'assets/js/*.js', 'assets/images/*', 'config.conf.j2'], {allowEmpty: true})
        .pipe(tar(package.name + '_' + package.version + '.tar'))
        .pipe(gzip())
        .pipe(gulp.dest('dist'))
);

gulp.task('clean', function () {
    return gulp.src(['dist', 'assets/css/*.css', 'assets/js/*.js'], {read: false, allowEmpty: true})
        .pipe(clean());
});

gulp.task('js', function() {
    // Single entry point to browserify
    return gulp.src('./assets/js/src/*.js', {allowEmpty: true})
        .pipe(browserify({
          insertGlobals : true,
          debug : true
        }))
        .pipe(gulp.dest('./assets/js/'))
});

gulp.task('sass', function () {
  return gulp.src('./assets/css/scss/**/*.scss', {allowEmpty: true})
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('build', gulp.series('clean', 'sass', 'js', 'tar'));