# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.6] - 2020-04-16
### Fixed
- [#2](https://gitlab.com/simtio/briques/simtio_dhcp/-/issues/2): config.conf.j2 no such file or directory

## [1.0.5] - 2020-04-16
### Added
- description
- french description

## [1.0.4] - 2019-12-20
### Fixed
- CI
### Changed
- dynamic version in docker-compose
- Fix dockerfile FROM tag