from core import BasePlugin, MenuItem, Page, Config
from bottle import request
import re
from jinja2 import Environment
import os
import sys


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()
        self.register_menu_item(MenuItem("DHCP", 'fas fa-cubes', "/plugins/dhcp/"))
        self.register_page(Page('', Page.METHOD_GET, self.action_index))
        self.register_page(Page('', Page.METHOD_POST, self.action_index_process))

        self.dns_plugin = ("dns" in Config().get("plugins"))

        self.config.set("config_file", Config().get('data_location') +
                        "/" +
                        self.get_plugin_type() +
                        "_" + self.get_plugin_name() +
                        "_dhcpd.conf")

    def action_index(self):
        if self.dns_plugin:
            domain = Config().get("plugins")['dns']['dns_domain']
            dns_servers = Config().get("worker_ip")
        else:
            domain = self.config.get("domain", "")
            dns_servers = self.config.get("dns_servers", "")
        return self.render("index", {
            "dns_plugin": self.dns_plugin,
            "subnet": self.config.get("subnet", ""),
            "netmask": self.config.get("netmask", ""),
            "domain": domain,
            "dns_servers": dns_servers,
            "gateway": self.config.get("gateway", ""),
            "range_start": self.config.get("range_start", ""),
            "range_end": self.config.get("range_end", ""),
            "dns_auto_add": self.config.get("dns_auto_add", False)
        })

    def action_index_process(self):
        request.body.read()
        # print(request.forms.keys())
        # dict_keys(['subnet', 'netmask', 'gateway'])

        if "subnet" in request.forms.keys():
            if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", request.forms.get('subnet')):
                self.config.set("subnet", request.forms.get('subnet'))

        if "netmask" in request.forms.keys():
            if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", request.forms.get('netmask')):
                self.config.set("netmask", request.forms.get('netmask'))

        if "gateway" in request.forms.keys():
            if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", request.forms.get('gateway')):
                self.config.set("gateway", request.forms.get('gateway'))

        if "domain" in request.forms.keys():
            self.config.set("domain", request.forms.get('domain'))

        if "dns_servers" in request.forms.keys():
            if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}( \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})?$", request.forms.get('dns_servers')):
                self.config.set("dns_servers", request.forms.get('dns_servers'))

        if "range_start" in request.forms.keys():
            if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", request.forms.get('range_start')):
                self.config.set("range_start", request.forms.get('range_start'))

        if "range_end" in request.forms.keys():
            if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", request.forms.get('range_end')):
                self.config.set("range_end", request.forms.get('range_end'))

        if "dns_auto_add" in request.forms.keys():
            self.config.set("dns_auto_add", True)
        else:
            self.config.set("dns_auto_add", False)

        self.deploy()

        return self.action_index()

    def deploy(self):
        if self.dns_plugin:
            domain = Config().get("plugins")['dns']['dns_domain']
            dns_servers = Config().get("worker_ip")
            domain_key = Config().get("plugins")['dns']['dns_key']
            dns_ip = dns_servers
        else:
            domain = self.config.get("domain", "")
            dns_servers = self.config.get("dns_servers", "")
            domain_key = None
            dns_ip = None

        with open(os.path.dirname(sys.modules[self.__class__.__module__].__file__) + "/config.conf.j2") as f:
            with open(self.config.get("config_file"), "w") as o:
                o.write(Environment().from_string(f.read()).render(
                    dns_servers=dns_servers,
                    domain=domain,
                    subnet=self.config.get("subnet"),
                    netmask=self.config.get("netmask"),
                    range_start=self.config.get("range_start"),
                    range_end=self.config.get("range_end"),
                    gateway=self.config.get("gateway"),
                    dns_auto_add=self.config.get("dns_auto_add"),
                    domain_key=domain_key,
                    dns_ip=dns_ip
                ))
                o.close()
            f.close()
        super(Plugin, self).deploy()
