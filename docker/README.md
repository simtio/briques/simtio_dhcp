# SIMTIO - isc-dhcp-server

## Starting the container
Automatic restart (also on-boot), binding dhcpd.conf and dhcpd.leases from host, dhcpd listening on eth0 :
```
docker run -d -v /etc/dhcp/dhcpd.conf:/etc/dhcp/dhcpd.conf --net=host --name=simtio-dhcp --restart=unless-stopped dhcp:latest eth0
```

For testing purposes :
```
docker run -v /etc/dhcp/dhcpd.conf:/etc/dhcp/dhcpd.conf --net=host --name=simtio-dhcp simtio-dhcp ens33
```

## Configuring /etc/dhcp/dhcpd.conf
A default sample is available in sample/dhcpd.conf.

Set default domain name & dns servers :
```
option domain-name "example.org";
option domain-name-servers ns1.example.org, ns2.example.org;
```

Lease time :
```
default-lease-time 600;
max-lease-time 7200;
```

Set as main dhcp server on the network :
```
authoritative;
```

Dynamic IP example :
```
subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.1 192.168.1.10;
  option routers 192.168.1.254;
  option domain-name-servers ns1.internal.example.org;
  option domain-name "internal.example.org";
}
```

Static IP example :
```
host cinnamon {
  hardware ethernet 00:0D:87:B3:AE:A6;
  fixed-address 192.168.1.5;
}
```

Setup dhcp/dns updates :
```
zone example.com. {
  primary 192.168.1.2;
  key example.com;
}
```
## Miscallenious
- Configuration files of isc-dhcp-server
  - `/etc/dhcp/dhcpd.conf`
  - `/etc/default/isc-dhcp-server`
- Leases folder : `/var/lib/dhcp`
- After editing `/etc/dhcp/dhcpd.conf` you need to restart the container
- Check isc-dhcp-server process : `ps ax | grep dhcpd`
- Check logs : `cat /var/log/syslog | grep dhcpd`
- Check configuration : `dhcpd -t`
- DHCP Server port : UDP 67
- DHCP Client port : UDP 68

## Sources
- https://wiki.debian.org/DDNS#DHCP_Server_Configuration
